import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import TasksScreen from "./tasks"
import { createStackNavigator } from 'react-navigation';
import {TaskEdit as TaskEditScreen} from './tasks/TaskEdit'
import {TaskList} from './tasks/TaskList'
import {TaskIndex as TaskIndexScreen} from './tasks/TaskIndex'
import { AuthStore } from './auth/AuthStore';
import { Login as LoginScreen} from './auth/Login';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      return (
        <AuthStore>      
          <RootStack/>
       </AuthStore>     
      );    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
const RootStack = createStackNavigator(
  {
    TaskIndex: {
      screen:TaskIndexScreen,
      navigationOptions: {
      header: null,
      }},
    TaskEdit: TaskEditScreen,
    Login:{
      screen:LoginScreen,
      navigationOptions: {
        header: null,
        }},
    MainApp:{
      screen:App,
      navigationOptions: {
        header: null,
        }},
  },
  {
    initialRouteName: 'TaskIndex',
  }
);
