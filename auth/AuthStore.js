import { httpApiUrl, wsApiUrl } from '../core/api';
import React, {Component} from 'react';
import {Provider} from './context';
import {getLogger} from "../core/utils";
import {Login} from "./Login"
import { AsyncStorage } from "react-native"

const log = getLogger('AuthProvider');

export class AuthStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      issue: null,
      token: null,
    };
  }

    componentDidMount=async()=> {
    var value = await AsyncStorage.getItem('token');
    if(value==null)
    {
        this.setState({token:null});
    }
    else
    {
        this.setState({token:value});
    }
  }

  storeData = async (token) => {
    try {
      await AsyncStorage.setItem('token', JSON.stringify({token:token}));
    } catch (error) {
      console.log("error saving token in storage");
    }
  }

  handleLogin = (username, password) => {
    fetch(`${httpApiUrl}/account/login`, {
      method: 'POST',   
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({username,password}), 
    })
    .then(response => response.json())
    .then((response) =>{
      this.storeData(response.Token);
      this.setState({ isLoading: false, token:response.Token }); 
    })    
    .catch(error =>{console.log(error); this.setState({ isLoading: false, issue: error })});  
  };

  render() {
    const {issue, isLoading, token} =this.state
    return (
      <Provider value={{ token }}>
        { token && !isLoading
          ? this.props.children
          : <Login 
              isLoading={ isLoading}
              issue={ issue}
              onSubmit={ (username,password)=> this.handleLogin(username,password) }/>} 
      </Provider>
    );
  }
}