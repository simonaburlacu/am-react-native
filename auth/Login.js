import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native';
import { Button, Label } from 'native-base';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state=
    {
       username:'',
       password:''
    }
  }

  render() { 
    return (      
      <View >
        <TextInput
            style={{height: 40, marginBottom:10, marginTop:50}}
            onChangeText={(text) => this.setState({username:text})}
            value={this.state.username}
            placeholder="Your username..."
        />

        <TextInput
            style={{height: 40, marginBottom:10, marginTop:10}}
            onChangeText={(text) => this.setState({password:text})}
            value={this.state.password}
            placeholder="Your password..."
            secureTextEntry={true}
        />

       <TouchableOpacity style={styles.buttonLogin} onPress={() =>{this.props.onSubmit(this.state.username, this.state.password);}}>     
        <Image
            style={styles.buttonImage} 
            source={require('./login.jpg')}
        />
    </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    buttonLogin:{
        width:120,
        height:100,
    },
    buttonImage:{
        width:120,
        height:50
        
    }

    
});