import React from 'react'
import TaskStore from './TaskStore'
import {TaskList} from './TaskList'

export default ()=>
(
    <TaskStore>
        <TaskList navigation={this.navigation}/>
    </TaskStore>
);