import { httpApiUrl, wsApiUrl } from '../core/api';
import React, {Component} from 'react';
import {Provider} from './context';
import {getLogger, issueToText} from "../core/utils";
import {Consumer} from './context';
const log = getLogger('TaskStore');
import { AsyncStorage, NetInfo } from "react-native"
import { Accelerometer } from 'expo';
var mAccel = 0.0;
var mAccelCurrent = 0.0;
var mAccelLast = 0.0;
var newmAccel = 0.0;
var newmAccelCurrent = 0.0;
var newmAccelLast = 0.0;

export class TaskStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      tasks: null,
      issue: null,
      updateFunction:this.updateTasks,
      eTagTasks:null,
      token:null,
      load:this.loadTasks.bind(this)
    };
    this.loadTasks= this.loadTasks.bind(this);
  }

  storeData = async (key,value) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.log(error+" error saving value in storage: "+key);
    }
  }

  async retrieveItem(key) {
    try {
      const retrievedItem =  await AsyncStorage.getItem(key);
      return retrievedItem;
    } catch (error) {
      console.log(error.message);
    }
    return
  }

  componentWillMount() {
    this._subscribe();
  }

  componentDidMount=async()=> {
    NetInfo.addEventListener(
      'connectionChange',
      this.handleConnectivityChange
    );

    Promise
    .all([
      this.retrieveItem('token'),
      this.retrieveItem('tasks'),
      this.retrieveItem('eTagTasks')  
    ])
    .then(results => {
      if(results[0]!=null)
      {
        this.setState({token: JSON.parse(results[0]).token});
      }

      if(results[1]!=null) 
      {
        this.setState({tasks:JSON.parse(results[1]).Tasks});
      }

      if(results[2]!=null) 
      {
        this.setState({eTagTasks:JSON.parse(results[2]).etag});
      }

      if(this.state.token!=null)
      {  
        this.loadTasks(this.state.eTagTasks);
      }
      else
      {
        console.log("unauthorized to load tasks");
      }
    
    })
    .catch(() => {
        this.setState({ error });
    });
  }

  componentWillUnmount() {
    this._unsubscribe();  
  }

  loadTasks(eTagTasks){
    var bearer = 'Bearer '+ this.state.token;
    var options=
    {
      'Content-Type': 'application/json',
      'Authorization': bearer
    }

    if(eTagTasks!=null)
    {
      options=
      {
        'Content-Type': 'application/json',
        'Authorization': bearer,
        'If-None-Match': eTagTasks
      }
    }
  
    this.setState({ isLoading: false, issue: null });
    fetch(httpApiUrl+"/tasks",
    {
      method:'GET',
      headers:options
    })
    .then(response=>{
      if(response.status==200)
      {
        this.storeData('eTagTasks',{etag:response.headers.map.etag});
        this.setState({eTagTasks:response.headers.map.etag});
        return response.json()
      }
      return ""
     })
    .then(json => {  
      if(json!="")
      {    
        this.storeData('tasks',json);     
        this.setState({tasks:json.Tasks});
      }  
        this.setState({ isLoading: false})
      })
    .catch(error =>{
        log(error);
        this.setState({ isLoading: false, issue: error })
      });
  }

  updateTasks = (description, id) => {
    NetInfo.getConnectionInfo()
    .then((connectionInfo) => {
      if(connectionInfo.type == "none" || connectionInfo.type == "unknown")
      {
        var newTasks=[];
        this.state.tasks.map((task)=>{
          if(task.Id == id){
            task.Description = description;
            newTasks.push(task);
          }
          else{
            newTasks.push(task);
          }
        });
        this.storeData("tasks",{Tasks:newTasks}).then(()=>{
        this.props.navigation.navigate('TaskIndex'); 
        this.setState({ isLoading: false, issue: "no internet" });
        });
      }
      else
      {
        var bearer = 'Bearer '+ this.state.token;   
        this.setState({ isLoading: false, issue: null });
        fetch(httpApiUrl+"/update",{method:'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': bearer
        },
        body: JSON.stringify({
          Description: description,
          Id:id}),
        })
          .then(response => {
            this.props.navigation.navigate('TaskIndex'); 
            this.setState({ isLoading: false});
            this.loadTasks();
          })
          .catch(error =>{
            log("error update", error);
            var newTasks=[];
            this.state.tasks.map((task)=>{
              if(task.id == id){
                task.Description = description;
                newTasks.push(task);
              }
              else{
                newTasks.push(task);
              }
            });
            this.storeData("tasks",newTasks);
            this.props.navigation.navigate('TaskIndex'); 
            this.setState({ isLoading: false, issue: error })});          
      }
    });
  };

  handleConnectivityChange=(connection)=>{
    if(connection.type != "none" && connection.type != "unknown" )
    {
        this.updateAllTasks();
    }
  }

  updateAllTasks= ()=>{
    this.state.tasks.map((task)=>{
      this.updateTasks(task.Description, task.Id);
    });
  }

  shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  _isShake= (accelerometerData)=>{
    let { x, y, z } = accelerometerData.accelerometerData;  
    newmAccelLast =  mAccelCurrent;
    newmAccelCurrent = Math.sqrt(x*x + y*y + z*z);
    var delta = newmAccelCurrent - newmAccelLast;
    newmAccel = mAccel * 0.9 + delta;
    if (newmAccel > 0.5 && this.state.tasks != null) {
        console.log("shaking");
        var tasks = this.state.tasks;
        tasks = this.shuffleArray(tasks);
        this.setState(tasks=tasks);
    }
    mAccel = newmAccel;
    mAccelLast = newmAccelLast;
    mAccelCurrent = newmAccelCurrent;
  }


_subscribe = () => {
    this._subscription = Accelerometer.addListener(accelerometerData => {
      this._isShake({ accelerometerData });
    });
  }
 
_unsubscribe = () => {
    this._subscription && this._subscription.remove();
    this._subscription = null;
  }

  render() {
    return (
      <Provider value={this.state}>
        {this.props.children}
      </Provider>
    );
  }
}

export default TaskStore;