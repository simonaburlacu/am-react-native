import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import {TaskList} from './TaskList'
import {TaskStore} from './TaskStore'
import {WebSocketService} from './../sockets/WebSocketService'

export class TaskIndex extends Component {
  constructor(props) {
    super(props);
    this.state=
    {
      webSocket:null
    }  
  }
  
  componentDidMount()
  {
    if(this.state.webSocket==null)
    {
      var ws=new WebSocketService();
      ws.startConnection("User");
      this.setState({webSocket:ws});
    }
  }

  render() {
    return (
    <TaskStore navigation={this.props.navigation} webSocket={this.state.webSocket}>
        <TaskList navigation={this.props.navigation} webSocket={this.state.webSocket}/>
    </TaskStore>
    );
  }
}