import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native';
import { Button, Label } from 'native-base';

export class TaskEdit extends Component {
  constructor(props) {
    super(props);
    this.state=
    {
        description:this.props.navigation.getParam('taskToEdit', '').Description,
        id:this.props.navigation.getParam('taskToEdit', '').Id
    }
  }

  onEditPress()
  {
      var updateFunct=this.props.navigation.getParam('updateFunction', '')
      updateFunct(this.state.description, this.state.id);
  }

  render() {
    var task=this.props.navigation.getParam('taskToEdit', 'some default value');
    return (   
      <View >
        <Text> Description: </Text>
        <TextInput
            style={{height: 40, marginBottom:10}}
            onChangeText={(text) => this.setState({description:text})}
            value={this.state.description}
        />

       <TouchableOpacity style={styles.buttonUpdate} onPress={() =>{this.onEditPress();}}>     
        <Image
            style={styles.buttonImage} 
            source={require('./update.png')}
        />
    </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    buttonUpdate:{
        width:120,
        height:100,
    },
    buttonImage:{
        width:120,
        height:50   
    } 
});