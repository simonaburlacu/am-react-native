import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

export class TaskView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    
    custom_style=styles.todoItemPriority3
      if (this.props.tasks.Priority===1)
        custom_style=styles.todoItemUrgent
      else if (this.props.tasks.Priority===2)
        custom_style=styles.todoItemPriority2
    

    return (      
      <View style={custom_style}>
        <Text style={styles.todoText}>{this.props.tasks.Id} {this.props.tasks.Description} {this.props.tasks.Deadline}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  todoItemUrgent: {
    backgroundColor:'#FE2E2E',
    flexDirection: 'row',
    height: 50,
    padding: 10,
    borderBottomColor: 'white',
    borderBottomWidth: 2
  },
  todoItemPriority2: {
    backgroundColor:'#FAAC58',
    flexDirection: 'row',
    height: 50,
    padding: 10,
    borderBottomColor:'white',
    borderBottomWidth: 2
  },
  todoItemPriority3: {
    backgroundColor:'#F4FA58',
    flexDirection: 'row',
    height: 50,
    padding: 10,
    borderBottomColor:'white',
    borderBottomWidth: 1
  },

  todoText: {
    flex: 1,
    fontSize: 19
  },
});