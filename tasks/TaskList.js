import React, {Component} from 'react';
import {Text, View,ScrollView, ActivityIndicator, StyleSheet, Button, TouchableOpacity, Image } from 'react-native';
import {getLogger, issueToText} from '../core/utils'
import {Consumer} from './context';
import {TaskView} from './TaskView'
import {List, ListItem, Right} from 'native-base'
const log=getLogger('TaskList');
import { AsyncStorage } from "react-native"
import { StackActions, NavigationActions } from 'react-navigation';

export class TaskList extends Component
{
    constructor(props)
    {
        super(props);
        this.onPressEdit=this.onPressEdit.bind(this);
        this.state=
        {
            message:'',
            messageRequested:false
        }
    }

    onPressEdit(task)
    {
        this.props.navigation.navigate('TaskEdit'); 
    }

    async removeItemValue(key) {
        try {
          await AsyncStorage.removeItem(key);
        }
        catch(exception) {
            console.log("error remove item "+exception);
        }
      }

    render(){
        return(
            <Consumer>
             {(state)=> {
                 //console.log("state",state)
                 if(this.props.webSocket!==null && !this.state.messageRequested){
                    this.props.webSocket.messageNotification((message) => {
                        console.log("Message from backend", message);
                        state.load(state.eTagTasks);
                    });
                    this.setState({messageRequested:true});
                }
                return(    
                <ScrollView>
                    <TouchableOpacity style={styles.buttonLogout} onPress={() =>{
                            this.removeItemValue('token');
                            this.removeItemValue('tasks');
                            this.removeItemValue('eTagTasks');
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'MainApp' })],
                              });
                              this.props.navigation.dispatch(resetAction);
                        }}>     
                        <Image
                            style={styles.buttonLogoutImage} 
                            source={require('./logout.png')}
                        />
                    </TouchableOpacity>

                    <ActivityIndicator size="large" animating={state.isLoading}/>
                    {state.issue && <Text>{issueToText(state.mapissue)}</Text>}
                    <List style={styles.list}>
                    {state.tasks && state.tasks.map((task,key) =>  

                        <ListItem style={styles.listItem}  key={key}>
                            <Text style={[styles.label, styles.bold]}>{task.Id} {task.Description}</Text>
                        
                            <TouchableOpacity key={task.Id} style={styles.buttonEdit} onPress={() =>{
                                this.props.navigation.navigate('TaskEdit',
                                {
                                    taskToEdit:task, 
                                    updateFunction: state.updateFunction
                                });
                            }}>     
                            <Image
                                style={styles.buttonEditImage} 
                                source={require('./edit.png')}
                            />
                            </TouchableOpacity>
                           
                        </ListItem>)}
                    </List>
                </ScrollView>

             )}}

            </Consumer>
        );
    }
}

const styles = StyleSheet.create({
    bold: {
        fontWeight: 'bold'
    },
    header: {
        flex: 1
    },
    label: {
        width: '100%'
    },
    list:{
        top:40,
        marginBottom:40
    },
    listItem: {
        marginLeft: 0,
        paddingLeft: 10,
        flexDirection: 'column',
        height:70     
    },
    buttonEdit:{
        width:100,
        height:100,
        position:'absolute',
        right:2,
    },
    buttonEditImage:{
        width:30,
        height:30
        
    },
    buttonLogout:{
        width:100,
        height:100,
        position:'absolute',
        right:2,
        top:20,
        bottom:20
    },
    buttonLogoutImage:{
        width:30,
        height:30,
        top:20,
        bottom:20
    },

    
});